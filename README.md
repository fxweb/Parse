# Linux Ubuntu - Parse Server Setup Automation (Parse Server, Parse Dashboard, MongoDb, SSL license, HTTPS Access, NGINX HTTP Server)

###Parse Server
- Parse GitHub: https://parseplatform.github.io/

##  Your Server + Parse Server + Parse Dashboard =  Creativity

Works with any Small VPS or Deticated Server with min requiments 1GB Ram + 10GB HD You need to follow the instructions. Simple steps.

### Prerequisites

A domain associated TO server

### Instructions

```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get -y install git bc
sudo git clone https://gitlab.com/fxweb/Parse.git
sudo sh Parse/parse-setup.sh
```

### What you get with this install
- Parse Server (SSL/HTTP only access)
- Parse Dashboard
- Mongo Db (No direct access)
- NGINX (SSL Web Server Only - More secure) - Folder to access files /usr/share/nginx/www/"Domain_Name"
- SSL License with HTTPS Access ONLY

You will be asked couple questions along the way. Once you are through, you will be asked to restart the server. Once restarted, you are all DONE.


NOTE: NOT RESPONSIBLE for this code at ALL. Install and use at your own risk.
